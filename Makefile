default: vendor-get

vendor-get:
	cd app; go mod vendor

vendor-update:
	cd app; go mod tidy
	make vendor-get

