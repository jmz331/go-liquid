package liquid

type Config struct {
	URL  string
	Port int
}
