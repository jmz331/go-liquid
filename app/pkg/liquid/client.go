package liquid

import (
	"context"
	"go-liquid/pkg/logger"
)

type Client struct {
	config *Config
}

func NewClient(c *Config) *Client {
	return &Client{c}
}

func (*Client) Ping(ctx context.Context) error {
	logger.Info("Liquid Ping()")
	return nil
}

func (*Client) Test(ctx context.Context) error {
	panic("implement me")
}
