package routes

import (
	"context"
	"go-liquid/pkg/logger"
	"net/http"
)

func getWalletInfo(w http.ResponseWriter, r *http.Request) {
	logger.Info("getWalletInfo")
	if err := liquidClient.Ping(context.Background()); err != nil {
		outputError(w, err)
		return
	}

	output(w, "Gorilla!\n")
}

func allocWallet(w http.ResponseWriter, r *http.Request) {
	output(w, "New Wallet has been created!\n")
}
