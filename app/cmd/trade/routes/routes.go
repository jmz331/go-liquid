package routes

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"go-liquid/pkg/liquid"
	"net/http"
)

var liquidClient *liquid.Client

func Init(r *mux.Router) error {

	liquidClient = liquid.NewClient(&liquid.Config{URL: "localhost", Port: 7400})
	//wallet info
	r.HandleFunc("/wallet", getWalletInfo).Methods("GET")
	r.HandleFunc("/wallet", allocWallet).Methods("POST")

	return nil
}

func outputError(w http.ResponseWriter, err error) {
	_, _ = w.Write([]byte("Gorilla!\n"))
}

func output(w http.ResponseWriter, data interface{}) {
	bytes, err := json.Marshal(data)
	if err != nil {
		outputError(w, err)
		return
	}
	_, err = w.Write(bytes)
	if err != nil {
		outputError(w, err)
		return
	}
}
