package main

import (
	"github.com/gorilla/mux"
	"go-liquid/cmd/trade/routes"
	"net/http"
)

func main() {
	r := mux.NewRouter()
	if err := routes.Init(r); err != nil {
		panic(err)
	}
	panic(http.ListenAndServe(":8080", r))
}
